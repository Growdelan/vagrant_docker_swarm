#!/bin/bash

sudo yum update -y
sudo yum install -y vim mc bash-completion epel-release git
sudo yum update -y
sudo yum install -y ansible

sudo systemctl stop firewalld && sudo systemctl disable firewalld

sudo setenforce 0

sudo cp /vagrant/script/config/config /etc/selinux/config
