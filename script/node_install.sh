#!/bin/bash

sudo yum update -y

sudo systemctl stop firewalld && sudo systemctl disable firewalld

sudo setenforce 0

sudo cp /vagrant/script/config/config /etc/selinux/config
